### preparer la db FluxBB :

Sur la table `forum_forums`, renommer *Archives* (pour *Archives 2* par ex.).

Supprimer les avatars corrompus :
- 993.jpg
- 13183.gif
- 18005.png
- 172953.png
- 194764.png
- 1686813.png
- 1712582.png

### commande :

```sh
./flarum app:import-from-fluxbb fluxbb_prod /srv/sites/org.ubuntu-fr.forum/avatars https://forum.ubuntu-fr.org --from-prefix forum_ --to-prefix flarum_ --resolved
```
(la première fois, pour nettoyer la base existante), puis :

```sh
./flarum app:import-from-fluxbb fluxbb_prod /srv/sites/org.ubuntu-fr.forum/avatars https://forum.ubuntu-fr.org --from-prefix forum_ --to-prefix flarum_ --resolved --resume
```
pour reprendre la progression en cours.

éventuellement avec option :

```sh
--restart-from topics
```
(après import des *users*)

```sh
--restart-from posts
```
(après import des *topics*)

```sh
--restart-from subscriptions
```
(après import des *posts*)

### temps :

- import users environ 4h
- import topics environ 4h
- import posts environ 40h
- import topic subscriptions 40 minutes

### résultat :

- users : un user en moins (sur flarum)
- 651233 topics / 651019 discussions // après clean 650929
- posts : pareil
- 541119 topic_subscriptions / 541022 discussion_user
- groups : un group en moins (group *New*) et ids changées
- bans : ?
- reports : 11 reports / 9 flags
- mentions : ?
