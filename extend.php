<?php

namespace ImportFluxBB;

use ImportFluxBB\Console\ImportFromFluxBB;
use Flarum\Extend;

return [
    (new Extend\Console())->command(ImportFromFluxBB::class),
];
