## FluxBB Importer

This project is forked from [archlinux-de/flarum-import-fluxbb](https://github.com/archlinux-de/flarum-import-fluxbb).

### Installation

#### Install Flarum

```sh
composer create-project flarum/flarum:v0.1.0-beta.16 flarum
```

#### Install and enable extensions

```sh
composer require crachecode/flarum-import-fluxbb:dev-main flarum/nicknames:0.1.0-beta.16.1 migratetoflarum/old-passwords:0.6.0
```

Enable *FluxBB Importer*, *Old Passwords* and *Nicknames* extensions from admin interface.

### Usage

For usage see :
```sh
./flarum app:import-from-fluxbb --help
```
