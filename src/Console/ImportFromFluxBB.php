<?php

namespace ImportFluxBB\Console;

use ImportFluxBB\Importer\Avatars;
use ImportFluxBB\Importer\Bans;
use ImportFluxBB\Importer\Categories;
use ImportFluxBB\Importer\Forums;
use ImportFluxBB\Importer\ForumSubscriptions;
use ImportFluxBB\Importer\Groups;
use ImportFluxBB\Importer\InitialCleanup;
use ImportFluxBB\Importer\PostMentionsUser;
use ImportFluxBB\Importer\Posts;
use ImportFluxBB\Importer\Reports;
use ImportFluxBB\Importer\Topics;
use ImportFluxBB\Importer\TopicSubscriptions;
use ImportFluxBB\Importer\Users;
use ImportFluxBB\Importer\Validation;
use Flarum\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ImportFromFluxBB extends AbstractCommand
{
    private Users $users;
    private Avatars $avatars;
    private Categories $categories;
    private Forums $forums;
    private Topics $topics;
    private Posts $posts;
    private TopicSubscriptions $topicSubscriptions;
    private ForumSubscriptions $forumSubscriptions;
    private Groups $groups;
    private Bans $bans;
    private Reports $reports;
    private PostMentionsUser $postMentionsUser;
    private Validation $validation;
    private InitialCleanup $initialCleanup;

    public function __construct(
        Users $users,
        Categories $categories,
        Forums $forums,
        Avatars $avatars,
        Topics $topics,
        Posts $posts,
        TopicSubscriptions $topicSubscriptions,
        ForumSubscriptions $forumSubscriptions,
        Groups $groups,
        Bans $bans,
        Reports $reports,
        PostMentionsUser $postMentionsUser,
        Validation $validation,
        InitialCleanup $initialCleanup
    ) {
        $this->users = $users;
        $this->categories = $categories;
        $this->forums = $forums;
        $this->avatars = $avatars;
        $this->topics = $topics;
        $this->posts = $posts;
        $this->topicSubscriptions = $topicSubscriptions;
        $this->forumSubscriptions = $forumSubscriptions;
        $this->groups = $groups;
        $this->bans = $bans;
        $this->reports = $reports;
        $this->postMentionsUser = $postMentionsUser;
        $this->validation = $validation;
        $this->initialCleanup = $initialCleanup;
        parent::__construct();
    }

    protected function configure()
    {
        // For inspiration see:
        // https://github.com/sineld/import-from-fluxbb-to-flarum
        // https://github.com/mondediefr/fluxbb_to_flarum
        // also https://github.com/pierres/ll/blob/fluxbb/FluxImport.php
        $this
            ->setName('app:import-from-fluxbb')
            ->setDescription('Import from FluxBB database')
            ->addArgument('fluxbb-database', InputArgument::OPTIONAL, '', 'fluxbb')
            ->addArgument('avatars-dir', InputArgument::OPTIONAL, '', '/fluxbb-avatars')
            ->addArgument('url', InputArgument::OPTIONAL, 'Forum URL without trailing slash', '/fluxbb-avatars')
            ->addOption('restart-from', null, InputOption::VALUE_REQUIRED, 'Restart import from step (user|avatar|topics|posts|subscriptions|groups|bans|reports|mentions|validation)', '')
            ->addOption('resolved', null, InputOption::VALUE_REQUIRED, 'Pattern for resolved tag)', '((re)?solved|r(é|e)solu)')
            ->addOption('resume', null, InputOption::VALUE_NONE, 'No database clean for tables users, topics and posts, resume progress')
            ->addOption('from-prefix', null, InputOption::VALUE_REQUIRED, 'FluxBB db table prefix', '')
            ->addOption('to-prefix', null, InputOption::VALUE_REQUIRED, 'Flarum db table prefix', '')
            ->addOption('clean', null, InputOption::VALUE_NONE, 'Remove out of sync discussions');
    }

    protected function fire()
    {
        ini_set('memory_limit', '16G');
        define('CAT_INCREMENT', 500);
        $stepArray = [
            'user' => 1,
            'avatar' => 2,
            'topics' => 4,
            'posts' => 5,
            'subscriptions' => 6,
            'groups' => 7,
            'bans' => 8,
            'reports' => 9,
            'mentions' => 10,
            'validation' => 11
        ];

        $step = $this->input->getOption('restart-from');
        if (empty($step)) {
            $step = 0;
        } else {
            if (isset($stepArray[$step])) {
                $step = $stepArray[$step];
            } else {
                $this->output->write("<error>${step} is not a step in list !</error>");
                return 1;
            }

        }

        if (!$this->input->getOption('resume')) {
            $this->initialCleanup->execute($this->output, $this->input, $step);
        }

        if ($step<=1) {
            $this->users->execute($this->output, $this->input);
        }
        if ($step<=2) {
            $this->avatars->execute($this->output,$this->input);
        }
        if ($step<=4) {
            $this->categories->execute($this->output, $this->input);
            $this->forums->execute($this->output, $this->input);
            $this->topics->execute(
                $this->output,
                $this->input,
                $this->input->getOption('resolved')
            );
        }
        if ($step<=5) {
            $this->posts->execute($this->output, $this->input);
        }
        if ($step<=6) {
            $this->topicSubscriptions->execute($this->output, $this->input);
            $this->forumSubscriptions->execute($this->output, $this->input);
        }
        if ($step<=7) {
            $this->groups->execute($this->output, $this->input);
        }
        if ($step<=8) {
            $this->bans->execute($this->output, $this->input);
        }
        if ($step<=9) {
            $this->reports->execute($this->output, $this->input);
        }
        if ($step<=10) {
            $this->postMentionsUser->execute($this->output, $this->input);
        }
        if ($step<=11) {
            $this->validation->execute($this->output, $this->input);
        }
    }
}