<?php

namespace ImportFluxBB\Importer;

use Illuminate\Database\ConnectionInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class TopicSubscriptions
{
    private ConnectionInterface $database;
    private string $fluxBBDatabase;

    public function __construct(ConnectionInterface $database)
    {
        $this->database = $database;
    }

    public function execute(OutputInterface $output, object $input)
    {
        $this->fluxBBDatabase = $input->getArgument('fluxbb-database');
        $this->fromPrefix = $input->getOption('from-prefix');
        $this->toPrefix = $input->getOption('to-prefix');

        $output->writeln('Importing topic_subscriptions...');

        $this->database->statement('SET FOREIGN_KEY_CHECKS=0');

        $topicSubscriptions = $this->database
            ->table($this->fluxBBDatabase.'.'.$this->fromPrefix.'topic_subscriptions')
            ->select(
                [
                    'user_id',
                    'topic_id'
                ]
            )
            ->orderBy('topic_id')
            ->get()
            ->all();

        $progressBar = new ProgressBar($output, count($topicSubscriptions));
        $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% | %elapsed:6s% / %estimated:-6s% | %memory:6s%');

        foreach ($topicSubscriptions as $topicSubscription) {
            $this->database
                ->table($this->toPrefix.'discussion_user')
                ->insertOrIgnore(
                    [
                        'user_id' => $topicSubscription->user_id,
                        'discussion_id' => $topicSubscription->topic_id,
                        'last_read_at' => null,
                        'last_read_post_number' => null,
                        'subscription' => 'follow'
                    ]
                );
            $progressBar->advance();
        }
        $this->database->statement('SET FOREIGN_KEY_CHECKS=1');
        $progressBar->finish();

        $output->writeln('');
    }
}
