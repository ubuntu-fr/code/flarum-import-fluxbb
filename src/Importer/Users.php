<?php

namespace ImportFluxBB\Importer;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class Users
{
    private ConnectionInterface $database;
    private string $fluxBBDatabase;

    public function __construct(ConnectionInterface $database)
    {
        $this->database = $database;
    }

    public function execute(OutputInterface $output, object $input)
    {
        $this->fluxBBDatabase = $input->getArgument('fluxbb-database');
        $this->fromPrefix = $input->getOption('from-prefix');
        $this->toPrefix = $input->getOption('to-prefix');

        $total = $this->database
            ->table($this->fluxBBDatabase.'.'.$this->fromPrefix.'users')
            ->count();

        $offset = $this->database
            ->table($this->toPrefix.'users')
            ->count();

        $users = $this->database
            ->table($this->fluxBBDatabase.'.'.$this->fromPrefix.'users')
            ->select(
                [
                    'id',
                    'group_id',
                    'username',
                    'password',
                    'email',
                    'title',
                    'realname',
                    'url',
                    'jabber',
                    'location',
                    'signature',
                    'disp_topics',
                    'disp_posts',
                    'email_setting',
                    'notify_with_post',
                    'auto_notify',
                    'show_smilies',
                    'show_img',
                    'show_img_sig',
                    'show_avatars',
                    'show_sig',
                    'timezone',
                    'dst',
                    'time_format',
                    'date_format',
                    'language',
                    'style',
                    'num_posts',
                    'last_post',
                    'last_search',
                    'last_email_sent',
                    'last_report_sent',
                    'registered',
                    'registration_ip',
                    'last_visit',
                    'admin_note',
                    'activate_string',
                    'activate_key'
                ]
            )
            ->where('username', '!=', 'Guest')
            ->orderBy('id')
            ->skip($offset)
            ->take(999999999)
            ->get()
            ->all();

        $progressBar = new ProgressBar($output, $total);
        $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% | %elapsed:6s% / %estimated:-6s% | %memory:6s%');

        $users = $this->rectifyUsernames($users, $output);
        $users = $this->renameDuplicates($users, $output);

        $output->writeln('Importing users...');
        
        $i = $offset;
        foreach ($users as $user) {
            $lastSeenAt = (new \DateTime())->setTimestamp($user->last_visit);
            $this->database
                ->table($this->toPrefix.'users')
                ->insert(
                    [
                        'id' => $user->id,
                        'username' => $user->username,
                        'nickname' => $user->nickname,
                        'email' => $user->email,
                        'is_email_confirmed' => $user->group_id == 0 ? 0 : 1,
                        'password' => '', // password will be migrated by migratetoflarum/old-passwords
                        'preferences' => $this->createPreferences($user),
                        'joined_at' => (new \DateTime())->setTimestamp($user->registered),
                        'last_seen_at' => $lastSeenAt,
                        'marked_all_as_read_at' => $lastSeenAt,
                        'read_notifications_at' => null,
                        'discussion_count' => $this->getDiscussionCount($user->id),
                        'comment_count' => $this->getCommentCount($user->id),
                        'read_flags_at' => null,
                        'suspended_until' => null,
                        'migratetoflarum_old_password' => $this->createOldPasswordHash($user->password)
                    ]
                );
            
            $progressBar->setProgress($i);
            $i++;
        }
        $progressBar->finish();

        $output->writeln('');
    }

    private function isValidUsername(string $username): bool
    {
        return preg_match('/^[a-z0-9_-]{3,100}$/i', $username);
    }

    /**
     * See https://github.com/migratetoflarum/old-passwords#sha1-bcrypt
     */
    private function createOldPasswordHash(string $passwordHash): ?string
    {
        $recrypt = true;
        if ($recrypt) {
            $data = [
                'type' => 'sha1-bcrypt',
                'password' => password_hash($passwordHash, PASSWORD_BCRYPT)
            ];
        } else {
            $data = [
                'type' => 'sha1',
                'password' => $passwordHash
            ];
        }

        return json_encode($data) ?? null;
    }

    private function createPreferences($user): ?string
    {
        $preferences = [];

        if ($user->auto_notify) {
            $preferences['followAfterReply'] = true;
        }

        if (!$preferences) {
            return null;
        }

        return json_encode($preferences);
    }

    private function rectifyUsernames(array $users, OutputInterface $output): array
    {
        $output->writeln("Rectifying usernames...");
        foreach ($users as &$user) {
            $user->nickname = $user->username;
            if (!$this->isValidUsername($user->username)) {
                $newUserName = preg_replace('/\.+/', '-', $user->username);
                $newUserName = trim($newUserName, '-');
                $newUserName = Str::slug($newUserName, '-', 'fr');

                if (strlen($newUserName) < 3) {
                    $newUserName = ($newUserName ? $newUserName : 'user') . '-' . $user->id;
                }

                if (!$this->isValidUsername($newUserName)) {
                    throw new \RuntimeException('Username still invalid: ' . $newUserName);
                }

                $user->username = mb_strtolower($newUserName);
            }
        }
        return $users;
    }

    private function renameDuplicates(array $users, OutputInterface $output): array
    {
        $total = count($users);

        $output->writeln("Renaming duplicate emails...");
        $emails = [];
        foreach ($users as $user) {
            $emails[$user->id] = mb_strtolower($user->email);
        }
        $count_duplicates = array_count_values($emails);
        $i = 0;
        foreach ($count_duplicates as $email => $count) {
            $i ++;
            if ($count > 1) {
                $ids = array_keys($emails, $email);
                $j = 1;
                foreach ($ids as $id) {
                    foreach ($users as &$user) {
                        if ($user->id == $id) {
                            if ($j != $count) {
                                if ($j < $count-1) $replacement = uniqid('_duplicate_');
                                else $replacement = '_duplicate';
                                $user->email = str_replace('@', $replacement.'@', $user->email);
                            }
                            $output->writeln($i.' / '.$total.' : '.$email.' -> '.$user->email);
                        }
                    }
                    $j ++;
                }
            }
        }
        unset($user);

        $output->writeln("Renaming duplicate usernames...");
        $usernames = [];
        foreach ($users as $user) {
            $usernames[$user->id] = mb_strtolower($user->username);
        }
        $count_duplicates = array_count_values($usernames);
        $i = 0;
        foreach ($count_duplicates as $username => $count) {
            $i ++;
            if ($count > 1) {
                $ids = array_keys($usernames, $username);
                $j = 1;
                foreach ($ids as $id) {
                    foreach ($users as &$user) {
                        if ($user->id == $id) {
                            if ($j != $count) {
                                if ($j < $count-1) $replacement = uniqid('_duplicate_');
                                else $replacement = '_duplicate';
                                $user->username = $user->username.$replacement;
                            }
                            $output->writeln($i.' / '.$total.' : '.$username.' -> '.$user->username);
                        }
                    }
                    $j ++;
                }
            }
        }
        return $users;
    }

    private function getDiscussionCount(int $userId): int
    {
        $topics = $this->database
            ->table($this->fluxBBDatabase.'.'.$this->fromPrefix.'topics')
            ->join($this->fluxBBDatabase.'.'.$this->fromPrefix.'posts', $this->fromPrefix.'topics.first_post_id', '=', $this->fromPrefix.'posts.id')
            ->select('topic_id')
            ->where($this->fromPrefix.'posts.poster_id', '=', $userId)
            ->get()
            ->all();
        return count($topics);
    }

    private function getCommentCount(int $userId): int
    {
        $posts = $this->database
            ->table($this->fluxBBDatabase.'.'.$this->fromPrefix.'posts')
            ->select('id')
            ->where('poster_id', '=', $userId)
            ->get()
            ->all();
        return count($posts);
    }
}
